package chainofresponcibility;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		
		ImmediateSuperior is = new ImmediateSuperior();
		UnitManager um = new UnitManager();
		CEO ceo = new CEO();

		System.out.println("What is your base salary?");
		double salary = sc.nextDouble();
		System.out.println("What is your target raise?");
		double payRaiseRequest = sc.nextDouble();

		is.setSuccessor(um);
		um.setSuccessor(ceo);
		
		boolean running = true;

		while (running) {
			is.HandleRequest(new PayRequest(salary, payRaiseRequest));
		}

	}
}
