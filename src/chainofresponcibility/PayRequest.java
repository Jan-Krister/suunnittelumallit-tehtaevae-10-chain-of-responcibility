package chainofresponcibility;

public class PayRequest {
	
	private double salary;
	private double payRaiseRequest;

	public PayRequest(double salary, double payRaiseRequest) {
		this.salary = salary;
		this.payRaiseRequest = payRaiseRequest;
	}

	public double getPayRaiseRequest() {
		return payRaiseRequest;
	}

	public void setPayRaiseRequest(double payRaiseRequest) {
		this.payRaiseRequest = payRaiseRequest;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}
