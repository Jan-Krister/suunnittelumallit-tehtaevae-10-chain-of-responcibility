package chainofresponcibility;

public abstract class PayRaiseHandler {

	protected PayRaiseHandler successor;

	public void setSuccessor(PayRaiseHandler successor) {
		this.successor = successor;
	}

	abstract public void HandleRequest(PayRequest request);

}
