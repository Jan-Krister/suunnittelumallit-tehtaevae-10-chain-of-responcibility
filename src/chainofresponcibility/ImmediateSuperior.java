package chainofresponcibility;

public class ImmediateSuperior extends PayRaiseHandler {

	private final double ALLOWABLE = 1.02;

	@Override
	public void HandleRequest(PayRequest request) {
		if ((request.getPayRaiseRequest() + request.getSalary()) / request.getSalary() < ALLOWABLE) {
			System.out.println("Immediate superior will approve the raise of " + request.getPayRaiseRequest() + "€");
			System.exit(0);
		} else if (successor != null) {
			successor.HandleRequest(request);
		}
	}

}
