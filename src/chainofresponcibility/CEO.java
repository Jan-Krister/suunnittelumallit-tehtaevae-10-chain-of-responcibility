package chainofresponcibility;

public class CEO extends PayRaiseHandler {

	private final double ALLOWABLE =  100;


	@Override
	public void HandleRequest(PayRequest request) {
		if ((request.getPayRaiseRequest() + request.getSalary()) / request.getSalary() < ALLOWABLE) {
			System.out.println("Immediate superior will approve the raise of " + request.getPayRaiseRequest() + "€");
		} else if (successor != null) {
			successor.HandleRequest(request);
		}
		System.exit(0);
	}

}
