package chainofresponcibility;

public interface PayRaise {

	public int handle();
	public int raisePay(double salary);
}
